from utils_comparison.comparison import player_team_similarity
from utils_comparison.team_dictionnary import team_dictionnary
from utils_comparison.open_image import get_user_image_path
from tkinter import *
from utils_comparison.crop_face_user import path_new_image

# def init():
#     window = Tk()
#     list = Listbox(window)
#     list.insert(1, "Installer la database.")
#     list.insert(2, "Continuer sans installer.")
#     window.mainloop()


if __name__ == "__main__":
    # init()

    creation_database = str(
        input(" Do you want to create the database ? (Yes/No)"))  # We offer the possibility to create the database
    if creation_database == "Yes":
        input("WARNING : This step can take a while... (press any key to start)")
        print("ok")
        team_dictionnary()
    # We will intereact with the user through windows.
    window = Tk()
    label = LabelFrame(
        window, text="tIAgre will find your football team", padx=20, pady=20)
    label.pack(fill="both", expand="yes")
    Label(label, text="You will have to select the image that you want to use. Close this window to continue.").pack()
    window.mainloop()
    user_img_path = get_user_image_path()
    new_img_path = path_new_image(user_img_path)
    # That is the main part of the program, using the function that get the best player and team relative to the user
    ((best_player, max_player), (best_team, max_team)
     ) = player_team_similarity(new_img_path)
    print('You should support ', best_team,
          ": you match at", max_team, "%",)
    print("The player that looks the most like you is :",
          best_player, ". You match at ", max_player, '%.')
