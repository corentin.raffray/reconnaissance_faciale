import keras_vggface
from keras_vggface import VGGFace
import cv2
import numpy as np
from os import listdir
import unidecode
import pickle


def team_dictionnary():
    with open("tiagre_find_your_football_team/teams.txt", 'r', encoding="utf-8") as file:
        lines = file.readlines()
        teams = {}
        by_two_lines = []  # read lines 2 by 2
        for line in lines:
            if len(by_two_lines) < 2:
                by_two_lines.append(line)
            else:
                team_name = by_two_lines[0][:-2]
                players = by_two_lines[1][:-1].strip(' ')
                players = unidecode.unidecode(players)  # to remove emphasis
                teams[team_name] = players.split(",")
                by_two_lines = [line]
    return (teams)


model = VGGFace(model='resnet50', include_top=False,
                input_shape=(224, 224, 3), pooling='avg')

if __name__ == "__main__":

    vectors_dico = {}
    dico = team_dictionnary()

    for team in dico:
        for player in dico[team]:
            vectors_dico[player] = []
            for player_image in listdir('tiagre_find_your_football_team\Data\{}\{}'.format(team, player)):
                img = cv2.imread('tiagre_find_your_football_team\Data\{}\{}\{}'.format(
                    team, player, player_image))
                resized = cv2.resize(
                    img, (224, 224), interpolation=cv2.INTER_AREA)
                resized = np.expand_dims(resized, axis=0)
                vectors_dico[player].append(model.predict(resized))
    # procédure pour sauvegarder
    file = open(
        "tiagre_find_your_football_team/utils_resnet/pickle_vectors_dictionnary", 'wb')
    pickle.dump(
        vectors_dico, file)
    file.close()

    # # procédure pour ouvrir
    # file_open = open(
    #     "tiagre_find_your_football_team/utils_resnet/pickle_vectors_dictionnary", 'rb')
    # obj = pickle.load(
    #     file_open)
    # file_open.close()
    # print(obj)
