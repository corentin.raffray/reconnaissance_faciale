import numpy as np
import cv2
import pickle
from keras_vggface import VGGFace


# we start by removing the final layer of the model
model = VGGFace(model='resnet50', include_top=False,
                input_shape=(224, 224, 3), pooling='avg')
img = input('what is the path of the image?')
# load the image
mat_initial = cv2.imread(img)
# changing the size of the image
mat = cv2.resize(mat_initial, (224, 224), interpolation=cv2.INTER_AREA)
# create an array containing the image
mat = np.expand_dims(mat, axis=0)
# treatment of the input image by the model without the final layer
mat = model.predict(mat)
# we open the file containing the output of the prediction of the model
# using our generated data
file_player = open(
    "tiagre_find_your_football_team/utils_resnet/pickle_vectors_dictionnary_player", 'rb')
obj = pickle.load(
    file_player)
file_player.close()
list_of_Players = list(obj.keys())
# initialisation of the player and the min norm of match
min_norm_player = np.linalg.norm(mat[0] - obj[list_of_Players[0]][0])
player_match = list_of_Players[0]
# searshing the min by comparing the input to all the images of the file
for player in list_of_Players:
    for image_player in obj[player]:
        if np.linalg.norm(mat[0]-image_player) < min_norm_player:
            min_norm_player = np.linalg.norm(mat-obj[player])
            player_match = player
print(player_match)


# we open the file containing the output of the prediction of the model
# using our generated data
file_team = open(
    "tiagre_find_your_football_team/utils_resnet/pickle_vectors_dictionnary_team", 'rb')
obj = pickle.load(
    file_team)
file_team.close()
list_of_teams = list(obj.keys())
min_norm_team = 0
# initialisation of the team and the min norm of match with this loop
for item in obj[list_of_teams[0]]:
    for player_img in item[1]:
        min_norm_team += np.linalg.norm(mat - player_img)
team_match = list_of_teams[0]
# searshing the min by comparing the input to all the images of the file
for team in list_of_teams:
    rate_of_similarity_team = 0
    for item in obj[team]:
        for player_img in item[1]:
            rate_of_similarity_team += np.linalg.norm(mat[0] - player_img)
    if rate_of_similarity_team < min_norm_team:
        team_match = team
        min_norm_team = rate_of_similarity_team/len(obj[team])
print(team_match)
