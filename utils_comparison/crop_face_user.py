import cv2
import numpy as np


def faces(input_image_path):
    # First of all, we identify the face and crop the picture to the face only
    face_cascade = cv2.CascadeClassifier(
        'facial_rec_tiagre/utils_cv/face_rec/haarcascade_frontalface_default.xml')
    img = cv2.imread(input_image_path)
    # converting the image to gray level which is easier to detect and treat
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # detection of all the faces in the image stored in a list
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in faces:
        visage = img[y:y+h, x:x+w]
    return visage


def path_new_image(input_image_path):
    img_face = np.array(
        faces(input_image_path))
    cv2.imwrite('user_image.jpg', img_face)
    # we finally save the new cropped imaged in order to have access to an easier path
    return ('user_image.jpg')
