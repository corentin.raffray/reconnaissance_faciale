import unidecode
import sys
print(sys.path)

# generation of dictionnary from the file equipes.txt where keys are the teams names
# and attributes are lists of team's player's names


def team_dictionnary():
    with open("tiagre_find_your_football_team/teams.txt", 'r', encoding="utf-8") as file:
        lines = file.readlines()
        teams = {}
        by_two_lines = []  # read lines 2 by 2
        for line in lines:
            if len(by_two_lines) < 2:
                by_two_lines.append(line)
            else:
                team_name = by_two_lines[0][:-2]
                players = by_two_lines[1][:-1].strip(' ')
                players = unidecode.unidecode(players)  # to remove emphasis
                teams[team_name] = players.split(",")
                by_two_lines = [line]
    return (teams)


# if __name__ == '__main__':
    # print(team_dictionnary()['Argentine'])  # To test if we removed emphasis
