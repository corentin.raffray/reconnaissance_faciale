from deepface import DeepFace
import cv2
import matplotlib.pyplot as plt
from os import listdir
import unidecode
import numpy as np


def team_dictionnary():
    with open("tiagre_find_your_football_team/teams.txt", 'r', encoding="utf-8") as file:
        lines = file.readlines()
        teams = {}
        by_two_lines = []  # read lines 2 by 2
        for line in lines:
            if len(by_two_lines) < 2:
                by_two_lines.append(line)
            else:
                team_name = by_two_lines[0][:-2]
                players = by_two_lines[1][:-1].strip(' ')
                players = unidecode.unidecode(players)  # to remove emphasis
                teams[team_name] = players.split(",")
                by_two_lines = [line]
    return (teams)


def comparison(image1, image2):
    # the entry of this function is two paths leading to two images which we'd like to compare
    #img1 = cv2.imread(image1)
    #img2 = cv2.imread(image2)
    print(image2)
    result = DeepFace.verify(image1, image2, enforce_detection=False)
    # The distance tells us how far apart are the two faces present in images
    distance = result['distance']
    return (1-distance)*100
    # we return the percentage of how much the pictures bring together


def comparison_person_vs_player(person_image_path, player_name, team_name):
    sum_match_player = 0
    # percentage sum of every picture of the player
    number_of_images = 0
    # number of images of the player
    for player_image in listdir("tiagre_find_your_football_team/Data/{}/{}".format(team_name, player_name)):
        if player_image != '.DS_Store':
            # we run the name of the files inside the directory in the entry of listdir
            number_of_images += 1
            sum_match_player += comparison(person_image_path, "tiagre_find_your_football_team/Data/{}/{}/{}".format(
                team_name, player_name, player_image))
            # we add the percentage of each comparison with the player's pictures
        #avg_match = sum_match_player / number_of_images
    return sum_match_player/number_of_images
    # final percentage of how much the person and player look alike


def comparison_person_vs_team(person_image_path, team_name):
    sum_match_team = 0
    # percentage sum of every picture of every player
    max_match_player = 0
    # storing the maximum of match of the person with the team's players
    number_of_players = len(
        listdir("tiagre_find_your_football_team/Data/{}".format(team_name)))
    for player_name in listdir("tiagre_find_your_football_team/Data/{}".format(team_name)):
        if player_name != '.DS_Store':
            new_match = comparison_person_vs_player(
            person_image_path, player_name, team_name)
            sum_match_team += new_match
            if new_match > max_match_player:
                max_match_player = new_match
                best_match = player_name
            # storing the team's best match of the players
    avg_match_team = sum_match_team / number_of_players
    # final percentage of how much the person looks alike the team's players
    return (avg_match_team, (best_match, max_match_player))


def player_team_similarity(person_image_path):
    teams = team_dictionnary().keys()
    # the list of all teams of the world cup
    max_team = 0
    max_player = 0
    # maximum of percentage of similarities of the person with a team and with a player from the world cup
    for team in teams:
        if team != '.DS_Store':
            # this is only for mac users who get this problem but doesn't affect the structure for the others
            result = comparison_person_vs_team(person_image_path, team)
            match_team = result[0]
            # percentage of similarity of the person with the team
            if match_team > max_team:
                max_team = match_team
                best_team = team
                # storing the team & percentage if it's the highest already
            team_best_player_match = result[1]
            team_best_player = team_best_player_match[0]
            team_best_match = team_best_player_match[1]
            if team_best_match > max_player:
                max_player = team_best_match
                best_player = team_best_player
                # storing the team & percentage if it's the highest already
    return ((best_player, max_player), (best_team, max_team))
