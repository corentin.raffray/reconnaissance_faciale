from tkinter.filedialog import askopenfilename


def get_user_image_path():
    path = askopenfilename()
    return path


if __name__ == "__main__":
    print(get_user_image_path())
