from google_images_download import google_images_download
from team_dictionnary import team_dictionnary


# We want to create a funnction that's importing the necessary data : the pictures of the players of the world cup.
# We will compare the picture of our user to those in the database
# This function works on databases in the format of dictionnaries with as keys the names of the teams and as attributes a list with the players of that team.
def import_data(database):
    teams = list(database.keys())
    for team in teams:
        if database[team] != []:
            # for every player in the database, we will search and download his pictures
            for player in database[team]:
                response = google_images_download.googleimagesdownload()
                arguments = {"keywords": "{}".format(player),
                             "limit": 1, "print_urls": False, 'output_directory': 'tiagre_find_your_football_team/Data/{}'.format(team)}
                paths = response.download(arguments)


if __name__ == '__main__':
    import_data(team_dictionnary())
