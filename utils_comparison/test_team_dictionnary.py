from pytest import *
from team_dictionnary import team_dictionnary
import os


def test_team_dictionnary():
    dico = team_dictionnary()
    assert " Lionel Messi" in dico["Argentine"]
    assert "Tunisie" in dico.keys()
    assert len(list(dico.keys())) == 32
    for team in dico.keys():
        assert len(dico[team]) <= 27


if __name__ == "__main__":
    test_team_dictionnary()
