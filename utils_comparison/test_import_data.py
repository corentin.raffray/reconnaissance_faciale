from import_data import import_data
from team_dictionnary import team_dictionnary
from os import listdir
from pytest import *


def test_import_data():
    dico = team_dictionnary()
    assert listdir("Data/")[:-1] == list(dico.keys())

    for team in dico.keys():
        print((len(list(dico[team])), len(listdir("Data/{}/".format(team)))))
        #assert len(list(dico[team])) == len(listdir("Data/{}/".format(team)))


if __name__ == '__main__':
    test_import_data()
