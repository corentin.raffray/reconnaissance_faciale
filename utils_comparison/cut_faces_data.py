import cv2
import os
from team_dictionnary import team_dictionnary
from tensorflow.keras.utils import load_img, img_to_array
from keras.preprocessing import image
import numpy as np
from keras_vggface import utils
from keras_vggface.vggface import VGGFace

def faces(input_image):
    faces_bonne_taille = []
    face_cascade = cv2.CascadeClassifier(
        'utils_comparison/haarcascade_frontalface_default.xml')
    img = cv2.imread(input_image)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # converting the image to gray level which is easier to detect and treat
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    # detection of all the faces in the image stored in a list
    for (x, y, w, h) in faces:
        visage = img[y:y+h, x:x+w]
        visage_bonne_taille = cv2.resize(visage, (224, 224))
        faces_bonne_taille.append(visage_bonne_taille)
    return faces_bonne_taille

def cut_faces():
    #this function browses all the images in data and cuts them to retrieve only the face 
    for nation in os.listdir('Data'):
        if nation != '_pycache_':
            for player in os.listdir('Data/{}'.format(nation)):
                i = 0
                for image in os.listdir('Data/{}/{}'.format(nation, player)):
                    i += 1
                    faces_img = faces(
                        'Data/{}/{}/{}'.format(nation, player, image))
                    # Because we want to take the pictures where there is only one face for more accuracy
                    if len(faces_img) == 1:
                        face1 = faces_img[0]
                        cv2.imwrite(
                            'Data_final/{}/{}/{}_{}.jpg'.format(nation, player, player, i), face1)

cut_faces()
