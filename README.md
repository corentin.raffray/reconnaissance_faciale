# Name of project
Find your world cup team and your player.

## authors

Yassine Blaiech
Hamza Letaief
Arthur Renout
Reda Belkahla
Corentin Raffray

## Description
You don't know which team to support ? Your national team isn't part of the world cup? Your national team got eliminated? Or you only want another team to support during the world cup? We found the solution ! Our program will find for you a team to support, you will only have to enter a picture of your face and the program will find the team that matches you! The match is not only based of the color of your skin but also on the geometry of your faces, on your eyes and even more ! 

How did we do that program?

First we created a database using the module google_image_download, we stored 10 pictures of every player of the upcoming world cup

Then we created the functions in utils that will compare your picture with the players using a module named DeepFace :
The DeepFace algorithm was trained on a labeled dataset of four million faces belonging to over 4'000 individuals, which was the largest facial dataset at the time of release. The approach is based on a deep neural network with nine layers.

We also used an other approach that consisted in using a pre trained neurol network named vggface. We cropped the last layer of the network so that we got only a vector, processed, by the network, that represents our image. We can apply that process on the whole database so that we get every vector representing our database. After that, we make the user's image go through the same neural network and we get the vector that represents it. Finally, we calculate the norm of the difference between our vector and every vector of the database and find the lowest : that's the nearest person.

Finally, we find the team that matches you the most and also the player so you can support him! (With the first approach first, and then as an amelioration we use the second approach and compare them)

## Usage

First you run the requirements.txt file : 

pip install -r requirements.txt 

Then you need to import the data (It will take some time):

python utils_comparison/import_data.py

Then you need to run the file find_your_team_main : 

python find_you_team_main.py

Then you enter the path of your picture, and you will finally get the team (and player) that matches you the most.

Then you enter the path of your picture, and you will finally get the team (and player) that matches you the most!

## contributing
Fork the Project

Create your Feature Branch (git checkout -b feature/AmazingFeature)

Commit your Changes (git commit -m 'Add some AmazingFeature')

Push to the Branch (git push origin feature/AmazingFeature)

Open a Pull Request

## Acknowledgments
Our resources for this project are:
https://gitlab-cw5.centralesupelec.fr/paolo.ballarini/cs_codingweeks_recfaciale_2022
https://viso.ai/computer-vision/deepface/
https://www.papit.fr/google-images-download/
https://github.com/Joeclinton1/google-images-download.git
https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_objdetect/py_face_detection/py_face_detection.html
